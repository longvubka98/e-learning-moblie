import {LogBox, Platform, SafeAreaView, StyleSheet} from 'react-native';
import 'react-native-gesture-handler';
import * as eva from '@eva-design/eva';
import {AuthProvider} from "./src/contexts/Auth";
import {ApplicationProvider, IconRegistry} from '@ui-kitten/components';
import {default as theme} from './assets/custom-theme.json';
import {Router} from "./src/Router";
import {EvaIconsPack} from '@ui-kitten/eva-icons';
import {MaterialIconPack} from './src/components/CustomMaterialIcon';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import {persistor, store} from './src/redux/store';
import * as Device from 'expo-device';
import * as Notifications from 'expo-notifications';
import React, {useEffect} from "react";
import AsyncStorage from "@react-native-async-storage/async-storage";

Notifications.setNotificationHandler({
    handleNotification: async () => ({
        shouldShowAlert: true,
        shouldPlaySound: true,
        shouldSetBadge: true,
    }),
});

export default function App() {
    LogBox.ignoreAllLogs(true);

    useEffect(() => {
        registerForPushNotificationsAsync().then(token => {
            if (token) {
                AsyncStorage.setItem('@expo_push_token', token);
            }
        });
    }, []);

    async function registerForPushNotificationsAsync() {
        let token;
        if (Device.isDevice) {
            const {status: existingStatus} = await Notifications.getPermissionsAsync();
            let finalStatus = existingStatus;
            if (existingStatus !== 'granted') {
                const {status} = await Notifications.requestPermissionsAsync();
                finalStatus = status;
            }
            if (finalStatus !== 'granted') {
                alert('Failed to get push token for push notification!');
                return;
            }
            token = (await Notifications.getExpoPushTokenAsync()).data;
        }
        if (Platform.OS === 'android') {
            Notifications.setNotificationChannelAsync('default', {
                name: 'default',
                importance: Notifications.AndroidImportance.MAX,
                vibrationPattern: [0, 250, 250, 250],
                lightColor: '#FF231F7C',
            });
        }
        return token;
    }

    return (
        <Provider store={store}>
            <PersistGate loading={null} persistor={persistor}>
                <SafeAreaView style={styles.container}>
                    <AuthProvider>
                        <IconRegistry icons={[EvaIconsPack, MaterialIconPack]}/>
                        <ApplicationProvider {...eva} theme={{...eva.light, ...theme}}>
                            <Router/>
                        </ApplicationProvider>
                    </AuthProvider>
                </SafeAreaView>
            </PersistGate>
        </Provider>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});
