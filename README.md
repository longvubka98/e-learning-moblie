## Installation ⚒️

> We recommend you use yarn

1. Install all packages in root directory

   ```bash
   yarn install

   # npm install [for npm]
   ```

2. Install expo-cli

   ```bash
   npm install --global expo-cli
   ```

3. Install expo client on your Android Phone at link: [Link Here](https://play.google.com/store/apps/details?id=host.exp.exponent&hl=vi&gl=US)
   - for iOS: try to search Expo Go at App Store


4. Run the app

   ```bash
   expo start
   ```

5. Open the app: open the Expo Client on your Android/iOS Phone and then scan QR code (QR code has shown in the opened window at step 5)

6.
   ```
   FB accounts to login:
   1. robin_uuzibku_test@tfbnw.net/123456Aa
   2. lyric_uipzjrv_hanh@tfbnw.net/123456Aa
   3. jennie_qwepaxo_pham@tfbnw.net/123456Aa
