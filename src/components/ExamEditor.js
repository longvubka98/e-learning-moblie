import React, { useState } from "react";
import {
    FlatList,
    Image,
    KeyboardAvoidingView,
    Platform,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
    View
} from "react-native";
import { StatusBar } from "expo-status-bar";
import { useSelector } from "react-redux";
import { Avatar, Button, Icon, Input, Layout, Radio, RadioGroup, Text, Select, IndexPath, SelectItem } from "@ui-kitten/components";
import * as ImagePicker from "expo-image-picker";
import { showFailedAlert, showSuccessAlert } from "../utils/alert";
import { createExam } from "../service/APIService";
import { CLASS_PICKER, SEMETER_PICKER, SUBJECT_PICKER } from "../constants/Constants";

const initialState = {
    message: "",
    postType: "Exam",
    grade: "",
    subjects: "",
    semester: "",
    totalQuestions: 0,
    duration: 0
}

function ExamEditor({ navigation }) {
    const { profile } = useSelector(state => state.userReducer);
    const [images, setImages] = useState([{ placeholder: true, id: 10000 }]);
    const [exam, setExam] = useState(initialState);
    const [answers, setAnswers] = useState([]);

    const [gradeIndex, setGradeIndex] = useState(-1);
    const [subjectIndex, setSubjectIndex] = useState(-1);
    const [semeterIndex, setSemeterIndex] = useState(-1);

    function postExam() {
        if (!exam.totalQuestions || !exam.duration || !images.length) {
            showFailedAlert("Please input total question, duration and pick images");
            return;
        }
        let payload = {};
        payload.exam = exam;
        payload.exam.answers = answers;
        payload.images = images.filter((item) => !item.placeholder);
        createExam(payload).then(() => {
            setExam(initialState);
            showSuccessAlert("Exam posted!");
            navigation.goBack();
        }).catch(() => {
            showFailedAlert("Failed to post exam");
        });
    }

    const onValueChange = (key, value) => {
        Object.assign(exam, { [key]: value });
    }

    const onEndEditing = () => {
        let newAnswers = [];
        for (let i = 0; i < exam.totalQuestions; i++) {
            newAnswers.push({ id: i, questionIndex: i });
        }
        setAnswers(newAnswers);
    }

    const onChangeAnswer = (index, value) => {
        const newState = [...answers];
        newState[index].answerIndex = value;
        setAnswers(newState);
    }

    const pickImage = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.Images,
            aspect: [4, 3],
            quality: 0.6,
            base64: false
        });
        if (!result.cancelled) {
            const img = {
                uri: Platform.OS === 'ios' ? result.uri.replace('file://', '') : result.uri,
                name: 'name.jpg',
                type: 'image/jpeg',
                id: new Date().getTime()
            };
            let newImg;
            if (images.length === 1) {
                newImg = images.slice(0, 0);
            } else {
                newImg = images.slice(0, images.length - 1);
            }
            newImg.push(img, { placeholder: true });
            setImages(newImg);
        }
    };

    const removeImage = (img) => {
        const newImages = images.filter(item => item.id !== img.id || item.placeholder);
        setImages(newImages);
    }

    const keyExtractor = (item, index) => index.toString();

    const renderPlacementItem = (item) => (
        <SelectItem title={item?.label} />
    );

    const renderImage = ({ item }) => {
        if (item.placeholder) {
            return (
                <View style={{ flex: 1 }}>
                    <TouchableOpacity onPress={pickImage} style={{ marginLeft: 5 }}>
                        <Icon
                            style={styles.icon}
                            pack={"material"}
                            name='image-plus'
                        />
                    </TouchableOpacity>
                </View>

            );
        }
        return (
            <View>
                <TouchableOpacity onPress={() => removeImage(item)}
                    style={{ position: "absolute", right: 0, top: 0, zIndex: 2 }}>
                    <Icon
                        style={{ width: 25, height: 25, color: 'red' }}
                        fill={"red"}
                        pack={"material"}
                        name='delete'
                    />
                </TouchableOpacity>
                <Image style={styles.image} source={{ uri: item.uri }} />
            </View>
        )
    }

    return (
        <KeyboardAvoidingView
            keyboardVerticalOffset={100}
            behavior={Platform.OS === "ios" ? "padding" : ""}
            style={{ flex: 1 }}
            contentContainerStyle={{ flex: 1 }}
        >
            <ScrollView style={{ flex: 1 }} contentContainerStyle={styles.container}>
                <StatusBar barStyle="light-content" />
                <View style={styles.profile}>
                    <Avatar size={"medium"} source={{ uri: profile?.avatarUrl }} />
                    <Text style={{ marginLeft: 15, fontWeight: "bold" }} category={"h6"}>{profile?.name}</Text>
                </View>
                <Layout style={{ flexDirection: "row", marginTop: 10 }} level='1'>
                    <Input
                        defaultValue={exam.totalQuestions}
                        onChangeText={(v) => onValueChange("totalQuestions", v)}
                        keyboardType="number-pad"
                        onEndEditing={onEndEditing}
                        style={styles.input}
                        placeholder='Total questions'
                    />
                    <Input
                        defaultValue={exam.duration}
                        onChangeText={(v) => onValueChange("duration", v)}
                        keyboardType="number-pad"
                        style={styles.input}
                        placeholder='Duration (minutes)'
                    />
                </Layout>
                <Layout style={{ flexDirection: "row", marginTop: 5 }} level='1'>
                    <Select
                        style={styles.select}
                        placeholder='Grade'
                        value={gradeIndex == -1 ? null : CLASS_PICKER[gradeIndex].label}
                        onSelect={index => {
                            setGradeIndex(index - 1)
                            onValueChange("grade", CLASS_PICKER[index - 1].value)
                        }}>
                        {CLASS_PICKER.map(renderPlacementItem)}
                    </Select>

                    <Select
                        style={styles.select}
                        placeholder='Subject'
                        value={subjectIndex == -1 ? null : SUBJECT_PICKER[subjectIndex].label}
                        onSelect={index => {
                            setSubjectIndex(index - 1)
                            onValueChange("subjects", SUBJECT_PICKER[index - 1].value)
                        }}>
                        {SUBJECT_PICKER.map(renderPlacementItem)}
                    </Select>
                </Layout>
                <Layout style={{ flexDirection: "row", marginTop: 5 }} level='1'>
                    <Select
                        style={styles.select}
                        placeholder='Semester'
                        value={semeterIndex == -1 ? null : SEMETER_PICKER[semeterIndex].label}
                        onSelect={index => {
                            setSemeterIndex(index - 1)
                            onValueChange("semester", SEMETER_PICKER[index - 1].value)
                        }}>
                        {SEMETER_PICKER.map(renderPlacementItem)}
                    </Select>
                </Layout>
                <Input
                    onChangeText={newText => onValueChange("message", newText)}
                    defaultValue={exam.message}
                    multiline={true}
                    textStyle={{ minHeight: 64 }}
                    style={{ marginTop: 10 }}
                    placeholder='What is on your mind?'
                />
                <View style={{ flex: 1, marginVertical: 15 }}>
                    {
                        answers.map((item, index) => {
                            return (
                                <View style={{ marginBottom: 5 }} key={index}>
                                    <Text category={"s1"}>Question {index + 1}</Text>
                                    <RadioGroup
                                        style={{ flexDirection: "row" }}
                                        selectedIndex={item.answerIndex}
                                        onChange={(value) => onChangeAnswer(index, value)}>
                                        <Radio status={"primary"} style={{ flex: 1 }}>A</Radio>
                                        <Radio status={"primary"} style={{ flex: 1 }}>B</Radio>
                                        <Radio status={"primary"} style={{ flex: 1 }}>C</Radio>
                                        <Radio status={"primary"} style={{ flex: 1 }}>D</Radio>
                                    </RadioGroup>
                                </View>
                            )
                        })
                    }
                </View>
                <FlatList
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                    data={images}
                    contentContainerStyle={{ alignItems: "center" }}
                    renderItem={renderImage}
                    keyExtractor={keyExtractor}
                />
                <Button status={"primary"}
                    style={{ marginTop: 20 }}
                    size={"medium"}
                    onPress={postExam}
                    accessoryLeft={(props) => <Icon name='plus-outline' {...props} />}>Post Exam</Button>
            </ScrollView>
        </KeyboardAvoidingView>
    );
}

const styles = StyleSheet.create({
    container: {
        flexGrow: 1,
        padding: 10,
        backgroundColor: "white",
    },
    profile: {
        flexDirection: "row",
        alignItems: "center",
    },
    icon: {
        width: 50,
        height: 50,
        color: "#6F6F6F"
    },
    input: {
        flex: 1,
        margin: 2,
    },
    image: {
        width: 150,
        height: 150,
        resizeMode: "cover",
        borderRadius: 5,
        marginTop: 10,
        marginRight: 10
    },
    select: {
        flex: 1,
        marginHorizontal: 2,
    },
});

export default React.memo(ExamEditor);
