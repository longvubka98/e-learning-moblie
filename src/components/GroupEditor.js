import React, {useState} from "react";
import {KeyboardAvoidingView, Platform, ScrollView, StyleSheet, View} from "react-native";
import {StatusBar} from "expo-status-bar";
import {Button, Icon, Input} from "@ui-kitten/components";
import {createGroup} from "../service/APIService";
import {showFailedAlert} from "../utils/alert";

function GroupEditor({navigation}) {
    const [name, setName] = useState("");
    const [desc, setDesc] = useState("");

    const onCreateGroup = () => {
        if (!name) return;
        createGroup({name, descriptions: desc}).then((r) => {
            navigation.navigate('HomeBottomTab', {
                screen: 'Group',
                params: {created: true}
            });
        }).catch(() => {
            showFailedAlert("Failed to create groups");
        });
    }

    return (
        <KeyboardAvoidingView
            keyboardVerticalOffset={100}
            behavior={Platform.OS === "ios" ? "padding" : ""}
            style={{flex: 1}}
            contentContainerStyle={{flex: 1}}
        >
            <ScrollView style={{flex: 1}} contentContainerStyle={styles.container}>
                <StatusBar barStyle="light-content"/>
                <View style={{flex: 1}}>
                    <Input
                        value={name}
                        onChangeText={(text) => setName(text)}
                        placeholder='Group Name.'
                    />
                    <Input
                        value={desc}
                        onChangeText={(text) => setDesc(text)}
                        multiline={true}
                        style={{marginTop: 30}}
                        textStyle={{minHeight: 45}}
                        placeholder='Group descriptions.'
                    />
                </View>
                <Button status={"primary"}
                        style={{marginTop: 30}}
                        size={"medium"}
                        onPress={onCreateGroup}
                        accessoryLeft={(props) => <Icon name='plus-outline' {...props}/>}>Create Group</Button>
            </ScrollView>
        </KeyboardAvoidingView>
    );
}

const styles = StyleSheet.create({
    container: {
        flexGrow: 1,
        padding: 10,
        backgroundColor: "white",
    },
    profile: {
        flexDirection: "row",
        alignItems: "center",
        paddingVertical: 10
    },
    icon: {
        marginTop: 20,
        width: 100,
        height: 100,
        alignSelf: "center",
        color: "#5670A1"
    }
});

export default React.memo(GroupEditor);
