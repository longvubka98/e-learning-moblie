import React, { useState } from "react";
import { Image, KeyboardAvoidingView, Platform, ScrollView, StyleSheet, TouchableOpacity, View } from "react-native";
import { StatusBar } from "expo-status-bar";
import { useSelector } from "react-redux";
import { Avatar, Button, Icon, Input, Text } from "@ui-kitten/components";
import * as ImagePicker from "expo-image-picker";
import { createGroupPost } from "../service/APIService";
import { showFailedAlert, showSuccessAlert } from "../utils/alert";

function GroupPostEditor({ navigation, route }) {
    const { profile } = useSelector(state => state.userReducer);
    const [image, setImage] = useState(null);
    const [message, setMessage] = useState("");

    const pickImage = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.Images,
            aspect: [4, 3],
            quality: 0.6,
            base64: false
        });
        if (!result.cancelled) {
            setImage(result.uri);
        }
    };

    const removeImage = () => {
        setImage(null);
    }

    const onCreatePost = () => {
        if (!message) return;
        let payload = {
            post: { message, postType: route.params.postType }
        };
        if (image) {
            payload.images = [{
                uri: Platform.OS === 'ios' ? image.replace('file://', '') : image,
                name: 'name.jpg',
                type: 'image/jpeg',
            }];
        }
        createGroupPost(route.params.groupId, payload).then((res) => {
            showSuccessAlert("Post created!");
            navigation.goBack();
        }).catch(() => {
            showFailedAlert("Failed to create post");
        });
    }

    return (
        <KeyboardAvoidingView
            keyboardVerticalOffset={100}
            behavior={Platform.OS === "ios" ? "padding" : ""}
            style={{ flex: 1 }}
            contentContainerStyle={{ flex: 1 }}
        >
            <ScrollView style={{ flex: 1 }} contentContainerStyle={styles.container}>
                <StatusBar barStyle="light-content" />
                <View style={styles.profile}>
                    <Avatar size={"large"} source={{ uri: profile?.avatarUrl }} />
                    <Text style={{ marginLeft: 15, fontWeight: "bold" }} category={"h6"}>{profile?.name}</Text>
                </View>
                <Input
                    value={message}
                    onChangeText={(text) => setMessage(text)}
                    multiline={true}
                    textStyle={{ minHeight: 64 }}
                    containerStyle={{ marginBottom: 30 }}
                    placeholder='What is on your mind?'
                />
                {!image && <TouchableOpacity style={{ flex: 1, justifyContent: "center" }} onPress={pickImage}>
                    <Icon
                        style={styles.icon}
                        pack={"material"}
                        name='image-plus'
                    />
                </TouchableOpacity>}
                {image &&
                    <View style={{ marginTop: 20 }}>
                        <TouchableOpacity onPress={removeImage}>
                            <Icon
                                style={{ width: 20, height: 20, alignSelf: "flex-end" }}
                                fill={"red"}
                                name='trash-outline'
                            />
                        </TouchableOpacity>
                        <Image source={{ uri: image }} style={{
                            width: "100%",
                            height: 250,
                            alignSelf: "center",
                            marginTop: 10,
                            resizeMode: "contain"
                        }} />
                    </View>
                }
                <Button status={"primary"}
                    onPress={onCreatePost}
                    style={{ marginTop: 30 }}
                    size={"medium"}
                    accessoryLeft={(props) => <Icon
                        name='plus-outline' {...props} />}>{route.params.postType === "Post" ? "Create Post" : "Create Exercise"}</Button>
            </ScrollView>
        </KeyboardAvoidingView>
    );
}

const styles = StyleSheet.create({
    container: {
        flexGrow: 1,
        padding: 10,
        backgroundColor: "white",
    },
    profile: {
        flexDirection: "row",
        alignItems: "center",
        paddingVertical: 10
    },
    icon: {
        marginTop: 20,
        width: 100,
        height: 100,
        alignSelf: "center",
        color: "#5670A1"
    }
});

export default React.memo(GroupPostEditor);
