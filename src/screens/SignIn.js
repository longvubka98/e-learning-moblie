import React, {useEffect} from "react";
import {Image, StyleSheet, View} from "react-native";
import {Button, Icon, Text} from '@ui-kitten/components';
import * as Facebook from 'expo-facebook';
import {showFailedAlert} from "../utils/alert";
import {socialLogin} from "../service/APIService";
import {useAuth} from "../contexts/Auth";
import AsyncStorage from "@react-native-async-storage/async-storage";

function SignIn() {
    const auth = useAuth();

    useEffect(() => {
        Facebook.initializeAsync({
            appId: '979268969354547',
        }).catch((e) => {
            console.log("failed to init fb: ", e);
        });
    }, []);

    const facebookLogin = async () => {
        try {
            const {type, token} = await Facebook.logInWithReadPermissionsAsync({permissions: ['public_profile', 'email']});
            if (type === 'success' && token) {
                const expoPushNotificationKey = await AsyncStorage.getItem('@expo_push_token');
                const payload = {
                    provider: "facebook",
                    token,
                    expoPushNotificationKey
                }
                socialLogin(payload).then((res) => {
                    if (res.data && res.data.accessToken) {
                        auth.setAccessToken(res.data.accessToken);
                    }
                });
            } else {
                showFailedAlert("Failed to login with Facebook");
            }
        } catch (error) {
            showFailedAlert("Failed to login with Facebook");
        }
    }

    return (
        <View style={styles.container}>
            <View style={styles.logo}>
                <Image style={{width: 150, height: 150}} source={require("../../assets/logo.png")}/>
                <Text category={"h6"} appearance={"default"}>Welcome to E-Learning!</Text>
            </View>

            <Button style={{marginTop: 70}} onPress={facebookLogin} status={"info"}
                    accessoryLeft={(props) => <Icon name='facebook' {...props}/>}>Login with Facebook</Button>
            <Button appearance={"outline"} style={{marginTop: 20}} status={"danger"}
                    accessoryLeft={(props) => <Icon name='google' {...props}/>}>Login with Google</Button>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        backgroundColor: "#FAFAFA",
        paddingHorizontal: 50,
    },
    logo: {
        alignItems: "center"
    }
});

export default React.memo(SignIn);
