import React, { useEffect, useState } from "react";
import { FlatList, Image, Platform, ScrollView, StyleSheet, TouchableOpacity, View } from "react-native";
import { Avatar, Divider, Icon, Text, useTheme } from '@ui-kitten/components';
import { showFailedAlert, showSuccessAlert } from "../utils/alert";
import { useDispatch, useSelector } from "react-redux";
import { fetchProfileData } from "../redux/actions";
import { getGroupInfo, votePost } from "../service/APIService";
import { BASE_URL } from "../constants/Constants";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { setStatusBarTranslucent } from 'expo-status-bar';
import { useRoute, useIsFocused } from "@react-navigation/native";

function GroupDetails({ navigation }) {
    const isFocused = useIsFocused();
    const dispatch = useDispatch();
    const route = useRoute();
    const theme = useTheme();

    const { profile } = useSelector(state => state.userReducer);
    const [group, setGroup] = useState(null);
    const [posts, setPosts] = useState([]);
    const [currentTime, setCurrentTime] = useState(new Date().getTime());
    const [loading, setLoading] = useState(false);
    const [page, setPage] = useState(0);

    useEffect(() => {
        if (isFocused) {
            loadGroup();
        }
    }, [isFocused]);

    const loadGroup = () => {
        getGroupInfo(route.params.group.id).then((r) => {
            setGroup(r.data);
            setPosts(r.data?.posts)
            console.log('r.data', r.data);
        }).catch(() => {
            showFailedAlert("Failed to fetch group info")
        });
    }

    const onClickLike = (id) => {
        votePost(id, "like").then((r) => {
            const index = posts.findIndex((item) => item.id === r.data.id);
            posts[index] = r.data;
            setPosts(posts);
            setCurrentTime(new Date().getTime());
            showSuccessAlert("You was like post");
        });
    }

    const onClickUnlike = (id) => {
        votePost(id, "unlike").then((r) => {
            const index = posts.findIndex((item) => item.id === r.data.id);
            posts[index] = r.data;
            setPosts(posts);
            setCurrentTime(new Date().getTime());
            showSuccessAlert("You was unlike post");
        });
    }

    const onPressPost = (post) => {
        navigation.navigate("PostDetails", { post });
    }

    const keyExtractor = (item, index) => index.toString();

    const renderPost = ({ item }) => {
        return (
            <View key={item.id} style={styles.postItem}>
                <View style={styles.statusCreator}>
                    <Avatar size={"large"} source={{ uri: item.user.avatarUrl }} />
                    <View style={{ justifyContent: "space-evenly", marginHorizontal: 15, flex: 1 }}>
                        <Text category={"s1"} style={{ fontWeight: "bold" }}>{item.user.name}</Text>
                        <Text category={"p2"}>{item.createdAt}</Text>
                    </View>
                    <View style={{ justifyContent: "center" }}>
                        <Icon
                            style={styles.smallIcon}
                            fill={item.postType === "Post" ? "red" : item.postType === "Exam" ? "#3771FE" : "#4DCE76"}
                            name={item.postType === "Post" ? "image-outline" : item.postType === "Exam" ? "file-text-outline" : "file-outline"}
                        />
                    </View>
                </View>
                <TouchableOpacity activeOpacity={.9} onPress={() => onPressPost(item)}>
                    <View style={styles.postMessage}>
                        <Text category={"p1"}>{item.message}</Text>
                    </View>
                    {
                        item['examInfo'] &&
                        <View style={{ paddingHorizontal: 20, paddingVertical: 8, flexDirection: "row" }}>
                            <View style={{ flex: 1, flexDirection: "row", alignItems: "center" }}>
                                <TouchableOpacity>
                                    <MaterialCommunityIcons name={"alarm"} size={20} />
                                </TouchableOpacity>
                                <Text style={{ marginLeft: 5 }} category={"s1"}>{item['examInfo'].duration} minutes</Text>
                            </View>
                            <View style={{ flex: 1, flexDirection: "row", alignItems: "center" }}>
                                <TouchableOpacity>
                                    <MaterialCommunityIcons name={"format-list-bulleted"} size={20} />
                                </TouchableOpacity>
                                <Text style={{ marginLeft: 5 }}
                                    category={"s1"}>{item['examInfo'].totalQuestions} questions</Text>
                            </View>
                            <View style={{ flex: 1, flexDirection: "row", alignItems: "center" }}>
                                <TouchableOpacity>
                                    <MaterialCommunityIcons name={"file-document-outline"} size={20} />
                                </TouchableOpacity>
                                <Text style={{ marginLeft: 5 }} category={"s1"}>{item['examInfo'].subjects}</Text>
                            </View>
                        </View>
                    }
                    {
                        item.postType !== "Exam" && item.images.length > 0 &&
                        <Image
                            source={{ uri: BASE_URL + "/public/images/" + item.images[0].picture }}
                            style={{
                                width: "100%",
                                height: 250,
                                alignSelf: "center",
                                marginTop: 10,
                                resizeMode: "contain",
                            }} />
                    }
                    {
                        item.postType === "Exam" &&
                        item.images.map((img) => {
                            return (
                                <Image
                                    source={{ uri: BASE_URL + "/public/images/" + img.picture }}
                                    style={{
                                        width: "100%",
                                        height: 250,
                                        alignSelf: "center",
                                        marginTop: 10,
                                        resizeMode: "contain",
                                    }} />
                            )
                        })
                    }
                </TouchableOpacity>
                <View style={styles.postReaction}>
                    <View style={styles.postReactionItem}>
                        <TouchableOpacity onPress={() => onClickLike(item.id)}>
                            <MaterialCommunityIcons name={"thumb-up"} size={20} color={theme['color-primary-default']} />
                        </TouchableOpacity>
                        <Text style={{ marginLeft: 5 }} category={"s1"}>{item.totalLike}</Text>
                    </View>
                    <View style={styles.postReactionItem}>
                        <TouchableOpacity onPress={() => onClickUnlike(item.id)}>
                            <MaterialCommunityIcons name={"thumb-down"} size={20} color={"black"} />
                        </TouchableOpacity>
                        <Text style={{ marginLeft: 5 }} category={"s1"}>{item.totalUnlike}</Text>
                    </View>
                    <View style={styles.postReactionItem}>
                        <MaterialCommunityIcons name={"comment-multiple"} size={20} color={"#BBB7B7"} />
                        <Text style={{ marginLeft: 5 }} category={"s1"}>{item.comments?.length}</Text>
                    </View>
                </View>
            </View>
        )
    }

    return (
        <View style={{ flex: 1 }}>
            <Divider />
            <FlatList
                style={{ flex: 1 }}
                contentContainerStyle={{ flexGrow: 1 }}
                keyExtractor={keyExtractor}
                extraData={currentTime}
                data={posts}
                renderItem={renderPost}
                ListEmptyComponent={
                    <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                        <Text category={"h6"} appearance={"hint"}>No posts</Text>
                    </View>
                }
                ListHeaderComponent={
                    <View>
                        <View style={styles.postEditor}>
                            <Avatar size={"large"} source={{ uri: profile?.avatarUrl }} />
                            <TouchableOpacity onPress={() => navigation.navigate("GroupPostEditor", { groupId: group?.id, postType: "Post" })}>
                                <Text style={{ marginLeft: 20 }} category={"s1"}>What is on your mind?</Text>
                            </TouchableOpacity>
                        </View>
                        <Divider />
                        <View style={[styles.postEditor, styles.postTypeContainer]}>
                            <TouchableOpacity onPress={() => navigation.navigate("GroupPostEditor", { groupId: group?.id, postType: "Post" })} activeOpacity={.5} style={styles.postType}>
                                <Icon
                                    style={styles.icon}
                                    fill='red'
                                    name='image-outline'
                                />
                                <Text style={{ marginLeft: 5 }} category={"s2"}>Post</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => navigation.navigate("GroupPostEditor", { groupId: group?.id, postType: "Exercise" })} activeOpacity={.5} style={styles.postType}>
                                <Icon
                                    style={styles.icon}
                                    fill='#4DCE76'
                                    name='file-outline'
                                />
                                <Text style={{ marginLeft: 5 }} category={"s2"}>Exercise</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => navigation.navigate("ExamEditor")} activeOpacity={.5}
                                style={styles.postType}>
                                <Icon
                                    style={styles.icon}
                                    fill='#3771FE'
                                    name='file-text-outline'
                                />
                                <Text style={{ marginLeft: 5 }} category={"s2"}>Exam</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                }
            />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexGrow: 1,
    },
    title: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        backgroundColor: 'white',
        paddingHorizontal: 20,
        paddingVertical: 10
    },
    postEditor: {
        flexDirection: "row",
        backgroundColor: 'white',
        alignItems: "center",
        paddingHorizontal: 20,
        paddingVertical: 10
    },
    postTypeContainer: {
        justifyContent: "space-between"
    },
    postType: {
        flexDirection: "row",
        alignItems: "center"
    },
    icon: {
        width: 32,
        height: 32,
    },
    smallIcon: {
        width: 20,
        height: 20,
    },
    postItem: {
        marginTop: 8,
        marginBottom: 5,
        backgroundColor: "white",
        paddingVertical: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    statusCreator: {
        flexDirection: "row",
        paddingHorizontal: 20,
    },
    postMessage: {
        marginTop: 10,
        paddingHorizontal: 20,
    },
    postReaction: {
        marginTop: 10,
        paddingHorizontal: 20,
        flexDirection: "row"
    },
    postReactionItem: {
        marginRight: 30,
        flexDirection: "row",
        alignItems: "center"
    }
});

export default React.memo(GroupDetails);
