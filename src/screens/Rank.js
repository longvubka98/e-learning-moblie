import React, {useState} from "react";
import {FlatList, ScrollView, StyleSheet, TouchableOpacity, View} from "react-native";
import {Avatar, Text} from '@ui-kitten/components';
import {getRanking} from "../service/APIService";
import {showFailedAlert} from "../utils/alert";
import {MaterialCommunityIcons} from "@expo/vector-icons";

function Rank({navigation}) {

    const [ranking, setRanking] = useState([]);

    React.useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            loadRanking();
        });
        return unsubscribe;
    }, [navigation]);

    const loadRanking = () => {
        getRanking().then(r => setRanking(r.data)).catch(() => showFailedAlert("Failed to load rank"));
    }

    const keyExtractor = (item, index) => index.toString();

    const renderRankItem = ({item}) => {
        return (
            <View key={item.id} style={styles.item}>
                <View style={{flexDirection: "row", alignItems: "center"}}>
                    <Avatar shape={"rounded"} size={"large"} source={{uri: item.user.avatarUrl}}/>
                    <Text category={"s1"} style={{fontWeight: "bold", marginLeft: 10}}>{item.user.name}</Text>
                </View>
                <View style={{flexDirection: "row", alignItems: "center"}}>
                    <MaterialCommunityIcons name={"heart"} size={20} color={"red"}/>
                    <Text category={"s1"} style={{marginLeft: 5}}>{item.totalCorrect}</Text>
                </View>
            </View>
        );
    }

    return (
        <ScrollView style={{flex: 1}} contentContainerStyle={styles.container}>
            <View style={styles.header}>
                <Text category={"h4"} style={{fontWeight: "bold"}}>Ranking</Text>
            </View>
            <FlatList
                style={{flex: 1, marginBottom: 5}}
                contentContainerStyle={{flexGrow: 1, marginHorizontal: 15}}
                keyExtractor={keyExtractor}
                ListEmptyComponent={
                    <View style={{flex: 1, justifyContent: "center", alignItems: "center"}}>
                        <Text category={"h6"} appearance={"hint"}>No data available</Text>
                    </View>
                }
                data={ranking}
                renderItem={renderRankItem}
            />
        </ScrollView>
    );
}

const styles = StyleSheet.create({
    container: {
        flexGrow: 1,
    },
    header: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        paddingHorizontal: 20,
        paddingVertical: 10
    },
    item: {
        flexDirection: "row",
        padding: 10,
        marginTop: 8,
        marginBottom: 5,
        backgroundColor: "white",
        justifyContent: "space-between",
        paddingVertical: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.15,
        shadowRadius: 3.84,
        elevation: 5,
        borderRadius: 10
    }
});

export default React.memo(Rank);
