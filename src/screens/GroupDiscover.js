import React, {useState} from "react";
import {FlatList, StyleSheet, View} from "react-native";
import {Avatar, Button, Icon, Text} from '@ui-kitten/components';
import {getWorldGroups, joinGroup} from "../service/APIService";
import {showFailedAlert, showSuccessAlert} from "../utils/alert";

function GroupDiscover({navigation}) {
    const [groups, setGroups] = useState([]);

    React.useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            loadGroups();
        });
        return unsubscribe;
    }, [navigation]);

    const loadGroups = () => {
        getWorldGroups().then((r) => {
            setGroups(r.data);
        });
    }

    const onClickJoin = (groupId) => {
        joinGroup(groupId).then(() => {
            showSuccessAlert("You has been join group");
        }).catch((error) => {
            showFailedAlert(error.response.data.message);
        });
    }

    const keyExtractor = (item, index) => index.toString();

    const renderGroup = ({item}) => {
        return (
            <View key={item.id} style={styles.item}>
                <Avatar style={{alignSelf: "center"}} shape={"rounded"} size={"large"} source={{uri: item.createdBy.avatarUrl}}/>
                <View style={{marginLeft: 15, flex: 1, flexDirection: "column", justifyContent: "space-around"}}>
                    <Text category={"s1"} style={{fontWeight: "bold"}}>{item.name}</Text>
                    <Text category={"p2"} appearance={"hint"}>{item.members.length} member(s)</Text>
                </View>
                <Button onPress={() => onClickJoin(item.id)} size={"large"} style={styles.button} status={"basic"} appearance='ghost'
                        accessoryLeft={(props) => <Icon {...props} pack={"material"} name='account-multiple-plus'/>}/>
            </View>
        );
    }
    return (
        <View style={styles.container}>
            <FlatList
                style={{flex: 1}}
                contentContainerStyle={{flexGrow: 1}}
                keyExtractor={keyExtractor}
                ListEmptyComponent={
                    <View style={{flex: 1, justifyContent: "center", alignItems: "center"}}>
                        <Text category={"h6"} appearance={"hint"}>No groups</Text>
                    </View>
                }
                data={groups}
                renderItem={renderGroup}
            />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingVertical: 10,
        paddingHorizontal: 20,
        backgroundColor: "white"
    },
    item: {
        flexDirection: "row",
        marginBottom: 20,
    },
    button: {
        margin: 2,
    },
});

export default React.memo(GroupDiscover);
