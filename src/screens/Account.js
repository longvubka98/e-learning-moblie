import React, {useState} from "react";
import {Image, StyleSheet, View} from "react-native";
import {Button, Icon, Text} from '@ui-kitten/components';
import {showSuccessAlert} from "../utils/alert";
import {useAuth} from "../contexts/Auth";
import {useDispatch, useSelector} from "react-redux";
import {logout} from "../redux/actions";
import {getMyExamResult} from "../service/APIService";

function Account({navigation}) {
    const auth = useAuth();
    const dispatch = useDispatch();

    const {profile} = useSelector(state => state.userReducer);
    const [exams, setExams] = useState([]);

    React.useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            loadExamResults();
        });
        return unsubscribe;
    }, [navigation]);

    const loadExamResults = () => {
        getMyExamResult().then((r) => {
            setExams(r.data);
        });
    }

    const signOut = () => {
        auth.signOut();
        dispatch(logout());
        showSuccessAlert("You has been signed out");
    }

    return (
        <View style={styles.container}>
            <View style={{padding: 20, flex: 1}}>
                <View style={{flex: 1}}>
                    <View style={{flexDirection: "row"}}>
                        <Image style={{width: 80, height: 80, borderRadius: 40}} source={{uri: profile?.avatarUrl}}/>
                        <View style={{marginLeft: 15, flex: 1, justifyContent: "space-evenly"}}>
                            <Text category={"h6"} style={{fontWeight: "bold"}}>{profile?.name}</Text>
                            <Text category={"s2"}>Joined At: {profile?.createdAt}</Text>
                        </View>
                    </View>
                    <View style={{flexDirection: "row"}}>
                        <View style={{
                            flex: 1,
                            marginTop: 10,
                            marginRight: 10,
                            borderRadius: 5,
                            padding: 10,
                            backgroundColor: "#DCDCDC"
                        }}>
                            <Text category={"h6"}>{exams.length}</Text>
                            <Text category={"s2"} appearance={"hint"}>Exam Taken</Text>
                        </View>
                        <View style={{
                            flex: 1,
                            marginTop: 10,
                            marginRight: 10,
                            borderRadius: 5,
                            padding: 10,
                            backgroundColor: "#DCDCDC"
                        }}>
                            <Text
                                category={"h6"}>{exams.reduce((total, current) => total + current['totalCorrect'], 0)}</Text>
                            <Text category={"s2"} appearance={"hint"}>Total Correct</Text>
                        </View>
                        <View style={{
                            flex: 1,
                            marginTop: 10,
                            marginRight: 10,
                            borderRadius: 5,
                            padding: 10,
                            backgroundColor: "#DCDCDC"
                        }}>
                            <Text
                                category={"h6"}>{exams.reduce((total, current) => total + current['totalWrong'], 0)}</Text>
                            <Text category={"s2"} appearance={"hint"}>Total Wrong</Text>
                        </View>
                    </View>
                </View>
                <Button size={"small"} onPress={signOut} status={"danger"}
                        accessoryLeft={(props) => <Icon name='log-out-outline' {...props}/>}>Sign Out</Button>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        margin: 10,
        backgroundColor: "white",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.2,
        shadowRadius: 3.84,
        elevation: 5,
    }
});

export default React.memo(Account);
