import React, { useEffect, useState } from "react";
import {
    FlatList,
    Image,
    KeyboardAvoidingView,
    Platform,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
    View
} from "react-native";
import { Avatar, Button, Divider, Input, Text, useTheme } from "@ui-kitten/components";
import { BASE_URL } from "../constants/Constants";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { commentPost, votePost } from "../service/APIService";
import { showFailedAlert, showSuccessAlert } from "../utils/alert";
import { useSelector } from "react-redux";

function PostDetails({ route, navigation }) {
    const theme = useTheme();
    const [post, setPost] = useState({});
    const [commentText, setCommentText] = useState("");
    const { profile } = useSelector(state => state.userReducer);

    useEffect(() => {
        setPost(route.params.post);
    }, []);

    React.useLayoutEffect(() => {
        if (route.params.post.postType === "Exam") {
            navigation.setOptions({
                headerRight: () => (
                    <Button size={"large"} onPress={() => navigation.navigate("TakeExam", { post: route.params.post })} status={"info"} appearance='ghost'>
                        Take Exam
                    </Button>
                )
            });
        }
    }, [navigation]);

    const onClickLike = (id) => {
        votePost(id, "like").then((r) => {
            setPost(r.data);
            showSuccessAlert("You was like post");
        });
    }

    const onClickUnlike = (id) => {
        votePost(id, "unlike").then((r) => {
            setPost(r.data);
            showSuccessAlert("You was unlike post");
        });
    }

    const onClickNewComment = () => {
        if (!commentText) return;
        commentPost(post.id, commentText).then((r) => {
            setPost(r.data);
            setCommentText("");
            showSuccessAlert("Post commented");
        }).catch(() => {
            showFailedAlert("Failed to comment on post");
        });
    }

    const keyExtractor = (item, index) => index.toString();

    const renderComment = ({ item, index }) => {
        return (
            <View key={item.id}>
                {
                    index > 0 &&
                    <Divider />
                }
                <View key={item.id} style={styles.commentItem}>
                    <Avatar size={"medium"} source={{ uri: item.user.avatarUrl }} />
                    <View style={{ justifyContent: "space-between", marginLeft: 15, flex: 1 }}>
                        <Text category={"s2"} style={{ fontWeight: "bold" }}>{item.user?.name}</Text>
                        <Text category={"p2"}>{item.message}</Text>
                    </View>
                </View>
            </View>
        );
    }

    return (
        <KeyboardAvoidingView style={{ flex: 1, marginTop: 10 }}
            behavior={Platform.OS === 'ios' ? 'padding' : null}
            keyboardVerticalOffset={70}>
            <ScrollView style={{ flex: 1 }} contentContainerStyle={styles.container}>
                <View style={styles.postItem}>
                    <View style={styles.statusCreator}>
                        <Avatar size={"large"} source={{ uri: post.user?.avatarUrl }} />
                        <View style={{ justifyContent: "space-evenly", marginLeft: 15 }}>
                            <Text category={"s1"} style={{ fontWeight: "bold" }}>{post.user?.name}</Text>
                            <Text category={"p2"}>{post.createdAt}</Text>
                        </View>
                    </View>
                    <View style={styles.postMessage}>
                        <Text category={"p1"}>{post.message}</Text>
                    </View>
                    {
                        post['examInfo'] &&
                        <View style={{ paddingHorizontal: 20, paddingVertical: 8, flexDirection: "row" }}>
                            <View style={{ flex: 1, flexDirection: "row", alignItems: "center" }}>
                                <TouchableOpacity>
                                    <MaterialCommunityIcons name={"alarm"} size={20} />
                                </TouchableOpacity>
                                <Text style={{ marginLeft: 5 }} category={"s1"}>{post['examInfo'].duration} minutes</Text>
                            </View>
                            <View style={{ flex: 1, flexDirection: "row", alignItems: "center" }}>
                                <TouchableOpacity>
                                    <MaterialCommunityIcons name={"format-list-bulleted"} size={20} />
                                </TouchableOpacity>
                                <Text style={{ marginLeft: 5 }}
                                    category={"s1"}>{post['examInfo'].totalQuestions} questions</Text>
                            </View>
                            <View style={{ flex: 1, flexDirection: "row", alignItems: "center" }}>
                                <TouchableOpacity>
                                    <MaterialCommunityIcons name={"file-document-outline"} size={20} />
                                </TouchableOpacity>
                                <Text style={{ marginLeft: 5 }} category={"s1"}>{post['examInfo'].subjects}</Text>
                            </View>
                        </View>
                    }
                    {
                        post.postType === "Post" && post.images?.length > 0 &&
                        <Image
                            source={{ uri: BASE_URL + "/public/images/" + post.images[0].picture }}
                            style={{
                                width: "100%",
                                height: 250,
                                alignSelf: "center",
                                marginTop: 10,
                                resizeMode: "contain",
                            }} />
                    }
                    {
                        (post.postType === "Exam" || post.postType === "Exercise") &&
                        post.images.map((img) => {
                            return (
                                <Image
                                    source={{ uri: BASE_URL + "/public/images/" + img.picture }}
                                    style={{
                                        width: "100%",
                                        height: 250,
                                        alignSelf: "center",
                                        marginTop: 8,
                                        resizeMode: "contain",
                                    }} />
                            )
                        })
                    }
                    <View style={styles.postReaction}>
                        <View style={styles.postReactionItem}>
                            <TouchableOpacity onPress={() => onClickLike(post.id)}>
                                <MaterialCommunityIcons name={"thumb-up"} size={20} color={"#E8213A"} />
                            </TouchableOpacity>
                            <Text style={{ marginLeft: 5 }} category={"s1"}>{post.totalLike}</Text>
                        </View>
                        <View style={styles.postReactionItem}>
                            <TouchableOpacity onPress={() => onClickUnlike(post.id)}>
                                <MaterialCommunityIcons name={"thumb-down"} size={20} color={"black"} />
                            </TouchableOpacity>
                            <Text style={{ marginLeft: 5 }} category={"s1"}>{post.totalUnlike}</Text>
                        </View>
                        <View style={styles.postReactionItem}>
                            <MaterialCommunityIcons name={"comment-multiple"} size={20} color={"#BBB7B7"} />
                            <Text style={{ marginLeft: 5 }} category={"s1"}>{post.comments?.length}</Text>
                        </View>
                    </View>
                </View>
                <Divider />
                <FlatList
                    style={{ flex: 1, marginBottom: 5, backgroundColor: "white" }}
                    contentContainerStyle={{ flexGrow: 1, marginBottom: 10 }}
                    keyExtractor={keyExtractor}
                    data={post.comments}
                    renderItem={renderComment}
                />
                <Divider />
                <View style={{
                    paddingTop: 10,
                    flexDirection: "row",
                    paddingHorizontal: 20,
                    alignItems: "center",
                    marginBottom: 5,
                }}>
                    <Avatar size={"medium"} source={{ uri: profile?.avatarUrl }} />
                    <Input
                        style={{ flex: 1, backgroundColor: "white", marginLeft: 5, borderRadius: 15 }}
                        placeholder='Add a comment'
                        value={commentText}
                        onChangeText={nextValue => setCommentText(nextValue)}
                    />
                    <MaterialCommunityIcons onPress={onClickNewComment} name={"send"} size={25}
                        style={{ marginLeft: 10 }}
                        color={theme['color-primary-default']} />
                </View>
            </ScrollView>
        </KeyboardAvoidingView>
    );
}

const styles = StyleSheet.create({
    container: {
        flexGrow: 1,
        backgroundColor: "white"
    },
    postItem: {
        marginTop: 8,
        backgroundColor: "white",
        paddingVertical: 10
    },
    statusCreator: {
        flexDirection: "row",
        paddingHorizontal: 20,
    },
    postMessage: {
        marginTop: 10,
        paddingHorizontal: 20,
    },
    postReaction: {
        marginTop: 10,
        paddingHorizontal: 20,
        flexDirection: "row"
    },
    postReactionItem: {
        marginRight: 30,
        flexDirection: "row",
        alignItems: "center"
    },
    commentItem: {
        paddingHorizontal: 20,
        paddingVertical: 5,
        flexDirection: "row",
        alignItems: "center"
    }
});

export default React.memo(PostDetails);
