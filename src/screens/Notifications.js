import React, {useEffect, useState} from "react";
import {FlatList, ScrollView, StyleSheet, View} from "react-native";
import {Avatar, Text} from '@ui-kitten/components';
import {getMyNotification} from "../service/APIService";

function Notifications({navigation}) {
    const [notifications, setNotifications] = useState([]);

    React.useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            loadNotifications();
        });
        return unsubscribe;
    }, [navigation]);

    const loadNotifications = () => {
        getMyNotification().then((r) => {
            setNotifications(r.data);
        });
    }

    const keyExtractor = (item, index) => index.toString();

    const renderNotification = ({item}) => {
        return (
            <View key={item.id} style={styles.item}>
                <Avatar size={"large"} source={{uri: item.sender.avatarUrl}}/>
                <View style={{justifyContent: "space-around", marginLeft: 15}}>
                    <Text category={"s1"} style={{fontWeight: "bold"}}>{item.title}</Text>
                    <Text category={"p2"} appearance={"hint"}>{item.message}</Text>
                </View>
            </View>
        );
    }

    return (
        <ScrollView style={{flex: 1}} contentContainerStyle={styles.container}>
            <View>
                <Text category={"h4"} style={{fontWeight: "bold"}}>Notifications</Text>
            </View>
            <FlatList
                style={{flex: 1, marginBottom: 5}}
                contentContainerStyle={{flexGrow: 1, marginVertical: 15}}
                keyExtractor={keyExtractor}
                ListEmptyComponent={
                    <View style={{flex: 1, justifyContent: "center", alignItems: "center"}}>
                        <Text category={"h6"} appearance={"hint"}>No notifications</Text>
                    </View>
                }
                data={notifications}
                renderItem={renderNotification}
            />
        </ScrollView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexGrow: 1,
        paddingVertical: 10,
        paddingHorizontal: 20,
        backgroundColor: "white"
    },
    item: {
        flexDirection: "row",
        marginBottom: 20
    }
});

export default React.memo(Notifications);
