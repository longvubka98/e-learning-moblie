import React, { useEffect, useState } from "react";
import { Image, ScrollView, StyleSheet, TouchableOpacity, View } from "react-native";
import { Avatar, Button, Icon, Radio, RadioGroup, Text } from '@ui-kitten/components';
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { BASE_URL } from "../constants/Constants";
import CountDown from 'react-native-countdown-component';
import { submitExamResult } from "../service/APIService";
import { showFailedAlert } from "../utils/alert";
import AwesomeAlert from 'react-native-awesome-alerts';

function TakeExam({ navigation, route }) {

    const [post, setPost] = useState({});
    const [started, setStarted] = useState(false);
    const [startedTime, setStartedTime] = useState(null);
    const [answers, setAnswers] = useState([]);
    const [alertVisible, setAlertVisible] = useState(false);
    const [result, setResult] = useState({});
    const [imageSize, setImageSize] = useState({ width: '100%', height: 300 })

    useEffect(() => {
        setPost(route.params.post);
    }, []);

    useEffect(() => {
        if (!post['examInfo']) return;

        let newAnswers = [];
        for (let i = 0; i < post['examInfo'].totalQuestions; i++) {
            newAnswers.push({ id: i, questionIndex: i, answerIndex: null });
        }
        setAnswers(newAnswers);
    }, [post]);

    const startExam = () => {
        setStartedTime(new Date());
        setStarted(true);
    }

    const onChangeAnswer = (index, value) => {
        const newState = [...answers];
        newState[index].answerIndex = value;
        setAnswers(newState);
    }

    const onTimeUp = () => {
        submitExam();
    }

    const submitExam = () => {
        const payload = {};
        payload.startedAt = startedTime;
        payload.answers = answers;
        submitExamResult(post.id, payload).then((r) => {
            setStarted(false);
            setResult(r.data);
            setAlertVisible(true);
        }).catch(() => {
            showFailedAlert("Failed to submit exam");
        });
    }

    return (
        <View style={{ flex: 1 }}>
            <View style={styles.timing}>
                {
                    !started &&
                    <Button status={"primary"}
                        style={{ margin: 10 }}
                        onPress={startExam}
                        disabled={started}
                        size={"medium"}
                        accessoryLeft={(props) => <Icon
                            name='play-circle-outline' {...props} />}>Start now!</Button>
                }
                {
                    started &&
                    <CountDown
                        until={post['examInfo'].duration * 60}
                        timeToShow={['M', 'S']}
                        separatorStyle={{ color: '#1CC625' }}
                        showSeparator
                        onFinish={onTimeUp}
                        size={18}
                    />
                }
            </View>
            <ScrollView style={{ flex: 1, marginTop: 5 }} contentContainerStyle={styles.container}>

                <View style={styles.postItem}>
                    <View style={styles.statusCreator}>
                        <Avatar size={"large"} source={{ uri: post.user?.avatarUrl }} />
                        <View style={{ justifyContent: "space-evenly", marginLeft: 15 }}>
                            <Text category={"s1"} style={{ fontWeight: "bold" }}>{post.user?.name}</Text>
                            <Text category={"p2"}>{post.createdAt}</Text>
                        </View>
                    </View>
                    <View style={styles.postMessage}>
                        <Text category={"p1"}>{post.message}</Text>
                    </View>
                    {
                        post['examInfo'] &&
                        <View style={{ paddingHorizontal: 20, paddingVertical: 8, flexDirection: "row" }}>
                            <View style={{ flex: 1, flexDirection: "row", alignItems: "center" }}>
                                <TouchableOpacity>
                                    <MaterialCommunityIcons name={"alarm"} size={20} />
                                </TouchableOpacity>
                                <Text style={{ marginLeft: 5 }} category={"s1"}>{post['examInfo'].duration} minutes</Text>
                            </View>
                            <View style={{ flex: 1, flexDirection: "row", alignItems: "center" }}>
                                <TouchableOpacity>
                                    <MaterialCommunityIcons name={"format-list-bulleted"} size={20} />
                                </TouchableOpacity>
                                <Text style={{ marginLeft: 5 }}
                                    category={"s1"}>{post['examInfo'].totalQuestions} questions</Text>
                            </View>
                            <View style={{ flex: 1, flexDirection: "row", alignItems: "center" }}>
                                <TouchableOpacity>
                                    <MaterialCommunityIcons name={"file-document-outline"} size={20} />
                                </TouchableOpacity>
                                <Text style={{ marginLeft: 5 }} category={"s1"}>{post['examInfo'].subjects}</Text>
                            </View>
                        </View>
                    }
                    {
                        post.postType === "Exam" &&
                        post.images.map((img) => {
                            return (
                                <Image
                                    source={{ uri: BASE_URL + "/public/images/" + img.picture }}
                                    style={{
                                        width: imageSize.width,
                                        height: imageSize.height,
                                        alignSelf: "center",
                                        marginTop: 8,
                                        resizeMode: "contain",
                                    }} />
                            )
                        })
                    }
                </View>
                <View style={{ flex: 1, padding: 10, marginTop: 10, backgroundColor: "white" }}>
                    {
                        answers.map((item, index) => {
                            return (
                                <View style={{ marginBottom: 5 }} key={index}>
                                    <Text category={"s1"}>Question {index + 1}</Text>
                                    <RadioGroup
                                        style={{ flexDirection: "row" }}
                                        onChange={(value) => onChangeAnswer(index, value)}
                                        selectedIndex={item.answerIndex}>
                                        <Radio status={"primary"} style={{ flex: 1 }}>A</Radio>
                                        <Radio status={"primary"} style={{ flex: 1 }}>B</Radio>
                                        <Radio status={"primary"} style={{ flex: 1 }}>C</Radio>
                                        <Radio status={"primary"} style={{ flex: 1 }}>D</Radio>
                                    </RadioGroup>
                                </View>
                            )
                        })
                    }
                </View>
            </ScrollView>
            <Button status={"primary"}
                style={{ margin: 10 }}
                disabled={!started}
                size={"medium"}
                onPress={submitExam}
                accessoryLeft={(props) => <Icon
                    name='plus-outline' {...props} />}>Submit Exam</Button>
            <AwesomeAlert
                show={alertVisible}
                contentContainerStyle={{ width: "75%" }}
                title="Congratulations!"
                message={`Total Questions: ${post?.['examInfo']?.totalQuestions}\nCorrect Answers: ${result['totalCorrect']}\nWrong Answers: ${result['totalWrong']}\n`}
                showConfirmButton={true}
                confirmText="OK"
                confirmButtonColor="#9155FD"
                confirmButtonStyle={{ width: 80, alignItems: "center" }}
                onConfirmPressed={() => {
                    setAlertVisible(false);
                }}
            />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flexGrow: 1
    },
    timing: {
        height: 75,
        justifyContent: "center",
        backgroundColor: "white",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    postItem: {
        marginTop: 8,
        backgroundColor: "white",
        paddingVertical: 10
    },
    statusCreator: {
        flexDirection: "row",
        paddingHorizontal: 20,
    },
    postMessage: {
        marginTop: 10,
        paddingHorizontal: 20,
    },
});

export default React.memo(TakeExam);
