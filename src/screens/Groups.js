import React, {useEffect, useState} from "react";
import {FlatList, ScrollView, StyleSheet, TouchableOpacity, View} from "react-native";
import {Avatar, Icon, Text} from '@ui-kitten/components';
import {useRoute} from "@react-navigation/native";
import {getMyGroups} from "../service/APIService";

function Groups({navigation}) {
    const route = useRoute();
    const [groups, setGroups] = useState([]);

    React.useEffect(() => {
        if (route.params?.created) {
            loadGroups();
        }
    }, [route]);

    React.useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            loadGroups();
        });
        return unsubscribe;
    }, [navigation]);

    useEffect(() => {
        loadGroups();
    }, []);

    const loadGroups = () => {
        getMyGroups().then((r) => {
            setGroups(r.data);
        });
    }

    const onPressDiscover = () => {
        navigation.navigate("GroupDiscover");
    }

    const onPressAddGroup = () => {
        navigation.navigate("GroupEditor");
    }

    const keyExtractor = (item, index) => index.toString();

    const renderGroup = ({item}) => {
        return (
            <TouchableOpacity onPress={() => navigation.navigate("GroupDetails", {group: item})}>
                <View key={item.id} style={styles.item}>
                    <Avatar shape={"rounded"} size={"large"} source={{uri: item.createdBy.avatarUrl}}/>
                    <View style={{justifyContent: "space-around", marginLeft: 15}}>
                        <Text category={"s1"} style={{fontWeight: "bold"}}>{item.name}</Text>
                        <Text category={"p2"} appearance={"hint"}>{item.members.length} member(s) - {item.posts.length} post(s)</Text>
                    </View>
                </View>
            </TouchableOpacity>
        );
    }

    return (
        <ScrollView style={{flex: 1}} contentContainerStyle={styles.container}>
            <View style={styles.header}>
                <Text category={"h4"} style={{fontWeight: "bold"}}>Groups</Text>
                <View style={{flexDirection: "row"}}>
                    <TouchableOpacity activeOpacity={.6} onPress={onPressAddGroup}>
                        <Icon
                            style={styles.icon}
                            fill='black'
                            name='plus-circle-outline'
                        />
                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={.6} onPress={onPressDiscover}>
                        <Icon
                            style={styles.icon}
                            fill='black'
                            name='globe-2-outline'
                        />
                    </TouchableOpacity>
                </View>
            </View>
            <FlatList
                style={{flex: 1, marginBottom: 5}}
                contentContainerStyle={{flexGrow: 1, marginVertical: 15}}
                keyExtractor={keyExtractor}
                ListEmptyComponent={
                    <View style={{flex: 1, justifyContent: "center", alignItems: "center"}}>
                        <Text category={"h6"} appearance={"hint"}>No groups</Text>
                    </View>
                }
                data={groups}
                renderItem={renderGroup}
            />
        </ScrollView>
    );
}

const styles = StyleSheet.create({
    container: {
        flexGrow: 1,
        backgroundColor: "white"
    },
    header: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        paddingHorizontal: 20,
        paddingVertical: 10
    },
    icon: {
        marginLeft: 15,
        width: 20,
        height: 20,
    },
    item: {
        flexDirection: "row",
        paddingHorizontal: 20,
        marginBottom: 20
    }
});

export default React.memo(Groups);
