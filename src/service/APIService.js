import {privateRequest, publicRequest} from "../utils/axiosInstance";
import AsyncStorage from "@react-native-async-storage/async-storage";
import {BASE_URL} from "../constants/Constants";

export const socialLogin = async (payload) => {
    return await publicRequest.post(`/auth/login/social`, payload);
}

export const fetchProfile = async () => {
    return await privateRequest.get(`/users/me`);
}

export const createPost = async (payload) => {
    let formData = new FormData();
    formData.append('post', JSON.stringify(payload.post));
    if (payload.images) {
        payload.images.forEach((item) => {
            formData.append("pictures", item);
        });
    }
    const token = await AsyncStorage.getItem("@access_token");
    const response = await fetch(BASE_URL + '/users/me/posts', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            "Content-Type": "multipart/form-data",
            "Authorization": "Bearer " + token,
        },
        body: formData
    });
    return await response.json();
}

export const createExam = async (payload) => {
    let formData = new FormData();
    formData.append('exam', JSON.stringify(payload.exam));
    if (payload.images && payload.images.length) {
        for (let i = 0; i < payload.images.length; i++) {
            delete payload.images[i].id;
            formData.append("pictures", payload.images[i]);
        }
    }
    const token = await AsyncStorage.getItem("@access_token");
    const response = await fetch(BASE_URL + '/users/me/examinations', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            "Content-Type": "multipart/form-data",
            "Authorization": "Bearer " + token,
        },
        body: formData
    });
    return await response.json();
}

export const createExercise = async (payload) => {
    let formData = new FormData();
    formData.append('exercise', JSON.stringify(payload.exercise));
    if (payload.images && payload.images.length) {
        for (let i = 0; i < payload.images.length; i++) {
            delete payload.images[i].id;
            formData.append("pictures", payload.images[i]);
        }
    }
    const token = await AsyncStorage.getItem("@access_token");
    const response = await fetch(BASE_URL + '/users/me/exercise', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            "Content-Type": "multipart/form-data",
            "Authorization": "Bearer " + token,
        },
        body: formData
    });
    return await response.json();
}

export const getPosts = async (page, size) => {
    return await privateRequest.get(`/users/posts?page=${page}&size=${size}`);
}

export const getMyNotification = async () => {
    return await privateRequest.get("/users/notifications/me");
}

export const votePost = async (postId, type) => {
    return await privateRequest.patch(`/users/posts/${postId}/vote/${type}`);
}

export const commentPost = async (postId, message) => {
    return await privateRequest.post(`/users/posts/${postId}/comment`, {message});
}

export const createGroup = async (payload) => {
    return await privateRequest.post(`/users/me/groups`, payload);
}

export const getMyGroups = async () => {
    return await privateRequest.get("/users/me/groups");
}

export const getWorldGroups = async () => {
    return await privateRequest.get("/users/groups");
}

export const joinGroup = async (groupId) => {
    return await privateRequest.patch(`/users/me/groups/${groupId}/join`);
}

export const createGroupPost = async (groupId, payload) => {
    let formData = new FormData();
    formData.append('post', JSON.stringify(payload.post));
    if (payload.images) {
        payload.images.forEach((item) => {
            formData.append("pictures", item);
        });
    }
    const token = await AsyncStorage.getItem("@access_token");
    const response = await fetch(BASE_URL + `/users/me/groups/${groupId}/posts`, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            "Content-Type": "multipart/form-data",
            "Authorization": "Bearer " + token,
        },
        body: formData
    });
    return await response.json();
}

export const getGroupInfo = async (id) => {
    return await privateRequest.get(`/users/groups/${id}`);
}

export const submitExamResult = async (postId, payload) => {
    return await privateRequest.post(`/users/me/examinations/${postId}/submit-result`, payload);
}

export const getMyExamResult = async () => {
    return await privateRequest.get(`/users/me/examinations/result`);
}

export const getRanking = async () => {
    return await privateRequest.get(`/users/examinations/ranking`);
}
