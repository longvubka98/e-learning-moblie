import {fetchProfile} from "../service/APIService";

export const FETCH_PROFILE = 'FETCH_PROFILE';
export const LOGOUT = 'LOGOUT';

export const fetchProfileData = () => {
    return async dispatch => {
        const response = await fetchProfile();
        if (response.data) {
            dispatch({
                type: FETCH_PROFILE,
                payload: response.data
            });
        } else {
            throw new Error("Unable to fetchProfile!");
        }
    };
};

export const logout = () => {
    return dispatch => {
        dispatch({
            type: LOGOUT,
        });
    }
}
