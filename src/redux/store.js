import {applyMiddleware, combineReducers, createStore} from 'redux';
import thunk from 'redux-thunk';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {persistReducer, persistStore} from 'redux-persist';
import userReducer from "./reducers";

const persistConfig = {
    key: 'root',
    storage: AsyncStorage,
};

const rootReducer = combineReducers({userReducer: persistReducer(persistConfig, userReducer)});

export const store = createStore(rootReducer, applyMiddleware(thunk));
export const persistor = persistStore(store);
