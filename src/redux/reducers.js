import {FETCH_PROFILE, LOGOUT} from './actions';

const initialState = {
    profile: null
};

function userReducer(state = initialState, action) {
    switch (action.type) {
        case FETCH_PROFILE:
            return {...state, profile: action.payload};
        case LOGOUT:
            return initialState;
        default:
            return state;
    }
}

export default userReducer;
