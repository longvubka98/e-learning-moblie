import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {useAuth} from "./contexts/Auth";
import {ActivityIndicator, View} from "react-native";
import {AppStack, AuthStack} from "./navigator";
import FlashMessage from "react-native-flash-message";
import {StatusBar} from "expo-status-bar";

export const Router = () => {
    const {token, loading} = useAuth();

    if (loading) {
        return (
            <View
                style={{
                    flex: 1,
                    justifyContent: 'center',
                }}>
                <ActivityIndicator color={'#01a0a0'} animating={true} size="small"/>
            </View>
        )
    }
    return (
        <NavigationContainer>
            <StatusBar/>
            {token ? <AppStack/> : <AuthStack/>}
            <FlashMessage position="top"
                          floating={true}
                          hideOnPress={true}
            />
        </NavigationContainer>
    );
}
