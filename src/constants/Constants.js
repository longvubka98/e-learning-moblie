export const BASE_URL = "http://192.168.10.106:8090/api";
export const CLASS_PICKER = [
    { label: 'Khối 6', value: '6' },
    { label: 'Khối 7', value: '7' },
    { label: 'Khối 8', value: '8' },
    { label: 'Khối 9', value: '9' },
    { label: 'Khối 10', value: '10' },
    { label: 'Khối 11', value: '11' },
    { label: 'Khối 12', value: '11' }
]

export const SUBJECT_PICKER = [
    { label: 'Toán', value: 'math' },
    { label: 'Ngữ văn', value: 'literature' },
    { label: 'Ngoại ngữ', value: 'english' },
    { label: 'Vật Lý', value: 'physics' },
    { label: 'Hoá học', value: 'chemistry' },
    { label: 'Lịch sử', value: 'history' },
    { label: 'Địa lý', value: 'geography' },
    { label: 'Giáo dục công dân', value: 'civic_education' },
    { label: 'Sinh học', value: 'biological' },
    { label: 'Tin học', value: 'information_technology' },
    { label: 'Công nghệ', value: 'technology' },
]

export const SEMETER_PICKER = [
    { label: 'Đầu kỳ I', value: '10' },
    { label: 'Giữa kỳ I', value: '11' },
    { label: 'Cuối kỳ I', value: '12' },
    { label: 'Đầu kỳ II', value: '20' },
    { label: 'Giữa kỳ II', value: '21' },
    { label: 'Cuối kỳ II', value: '22' },
]
