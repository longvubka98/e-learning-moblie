import { createStackNavigator } from '@react-navigation/stack';
import SignIn from "./screens/SignIn";
import Home from "./screens/Home";
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { MaterialCommunityIcons } from "@expo/vector-icons";
import Rank from "./screens/Rank";
import Groups from "./screens/Groups";
import Account from "./screens/Account";
import React from "react";
import PostEditor from "./components/PostEditor";
import PostDetails from "./screens/PostDetails";
import Notifications from "./screens/Notifications";
import GroupEditor from "./components/GroupEditor";
import GroupDiscover from "./screens/GroupDiscover";
import GroupDetails from "./screens/GroupDetails";
import { Avatar } from "@ui-kitten/components";
import GroupPostEditor from "./components/GroupPostEditor";
import ExamEditor from "./components/ExamEditor";
import TakeExam from "./screens/TakeExam";
import ExerciseEditor from './components/ExerciseEditor';

const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();

const AppStack = () => {
    return (
        <Stack.Navigator screenOptions={{ headerBackTitle: 'Back' }}>
            <Stack.Screen name="HomeBottomTab" options={{ headerShown: false }} component={HomeBottomTab} />
            <Stack.Screen name="PostDetails" options={{ title: "Post Details" }} component={PostDetails} />
            <Stack.Screen name="TakeExam" options={{ title: "Examination", headerBackTitle: "Back" }} component={TakeExam} />
            <Stack.Screen name="GroupDiscover" options={{ title: "Group Discover" }} component={GroupDiscover} />
            <Stack.Screen name="GroupDetails" options={({ route }) => ({
                title: route.params.group.name,
                headerBackTitle: "Back",
                headerRight: () => (
                    <Avatar style={{ marginRight: 20 }} size={"small"}
                        source={{ uri: route.params.group.createdBy.avatarUrl }} />
                ),
            })}
                component={GroupDetails} />
            <Stack.Group screenOptions={{ presentation: "modal", headerBackTitle: "Back" }}>
                <Stack.Screen name="PostEditor" options={({ route }) => ({ title: "Create Post" })} component={PostEditor} />
                <Stack.Screen name="ExerciseEditor" options={({ route }) => ({ title: "Create Exercise" })} component={ExerciseEditor} />
                <Stack.Screen name="GroupEditor" options={{ title: "Create Group" }} component={GroupEditor} />
                <Stack.Screen name="GroupPostEditor" options={{ title: "Create Post" }} component={GroupPostEditor} />
                <Stack.Screen name="ExamEditor" options={{ title: "Create Exam" }} component={ExamEditor} />
            </Stack.Group>
        </Stack.Navigator>
    )
}

const HomeBottomTab = () => {
    return (
        <Tab.Navigator initialRouteName={"Home"}>
            <Tab.Screen name="Home" component={Home} options={{
                headerShown: false,
                tabBarActiveTintColor: "#5389F4",
                tabBarIcon: ({ color, size }) => (
                    <MaterialCommunityIcons name="home" color={color} size={size} />
                ),
            }} />
            <Tab.Screen name="Rank" component={Rank} options={{
                headerShown: false,
                tabBarActiveTintColor: "#5389F4",
                tabBarIcon: ({ color, size }) => (
                    <MaterialCommunityIcons name="podium" color={color} size={size} />
                ),
            }} />
            <Tab.Screen name="Group" component={Groups} options={{
                headerShown: false,
                tabBarActiveTintColor: "#5389F4",
                tabBarIcon: ({ color, size }) => (
                    <MaterialCommunityIcons name="account-group" color={color} size={size} />
                ),
            }} />
            <Tab.Screen name="Notification" component={Notifications} options={{
                headerShown: false,
                tabBarActiveTintColor: "#5389F4",
                tabBarIcon: ({ color, size }) => (
                    <MaterialCommunityIcons name="bell" color={color} size={size} />
                ),
            }} />
            <Tab.Screen name="Account" component={Account} options={{
                headerShown: false,
                tabBarActiveTintColor: "#5389F4",
                tabBarIcon: ({ color, size }) => (
                    <MaterialCommunityIcons name="account" color={color} size={size} />
                ),
            }} />
        </Tab.Navigator>
    );
};

const AuthStack = () => {
    return (
        <Stack.Navigator screenOptions={{ headerBackTitle: 'Back' }}>
            <Stack.Screen options={{ headerShown: false }} name="signIn" component={SignIn} />
        </Stack.Navigator>
    );
};

export { AuthStack, AppStack };
