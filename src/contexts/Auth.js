import React, {createContext, useContext, useEffect, useState} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';

const AuthContext = createContext({});

const AuthProvider = ({children}) => {
    const [token, setToken] = useState("");
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        loadStorageData();
    }, []);

    const loadStorageData = async () => {
        try {
            const token = await AsyncStorage.getItem('@access_token');
            if (token) {
                setToken(token);
            }
        } finally {
            setLoading(false);
        }
    }

    const setAccessToken = async (token) => {
        if (!token) throw new Error('token must be used within an AuthProvider');
        setToken(token);
        AsyncStorage.setItem('@access_token', token);
    };

    const signOut = () => {
        setToken(undefined);
        AsyncStorage.removeItem('@access_token');
    };

    return (
        <AuthContext.Provider value={{token, loading, setAccessToken, signOut}}>
            {children}
        </AuthContext.Provider>
    );
};

const useAuth = () => {
    const context = useContext(AuthContext);
    if (!context) {
        throw new Error('useAuth must be used within an AuthProvider');
    }
    return context;
}

export {AuthContext, AuthProvider, useAuth};
